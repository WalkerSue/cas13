#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
# install.packages(c("EnvStats","ggplot2","reshape2","ggridges","gridExtra","grid") , lib = "~/R/x86_64-redhat-linux-gnu-library/3.5", dependencies = T)
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggridges, quietly = T))
suppressMessages(library(EnvStats, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(grid, quietly = T))

Calculate_gMFI=function(a){
  m = abs(min(a))+1
  return(geoMean(a+m))}

GetTreatment = function(x){tmp = strsplit(x , split = "_")[[1]]
paste0(tmp[1:(length(tmp)-1)], collapse = "_")}

GetRep = function(x){tmp = strsplit(x , split = "_")[[1]]
return(tmp[length(tmp)])}

GetGuideInfo <- function(x,y = 2){strsplit(as.character(x), split = "_")[[1]][y]}
GetName <- function(x,y = 1:2){paste0(strsplit(as.character(x), split = "_")[[1]][y], collapse = "")}

Assign_Info <- function(x, ID = ID.mat,y=2){
  row = which( ID[,1] == x)
  return(ID[row,y])
}
SelectTopCells = function(x,p = P, side = "top"){
  if (side == "top"){
    sort(x,decreasing = T)[1:round(length(x)*p)]
  }else{
    sort(x,decreasing = F)[1:round(length(x)*p)]
  }
}

CalcPercGFPpos = function(x,Q=q){
  (table(x > Q)/length(x))["TRUE"]
}


###### execute ######################################
# load data
data = read.delim( file = "./FACS_data/FACS_GFPScreenValidation.csv", sep=",",  header = T,  stringsAsFactors = F)
colnames(data) = gsub("X","G",colnames(data))
data = data[,-grep("G103_2",colnames(data))]
colnames(data) = gsub("G103_1","G103",colnames(data))


############### Seed confirmation ########

# matrix with pLCR to Cas13 protein naming
ID.mat <- cbind(c("WT", "NT","G100","G101","G102","G103","G104","G105"),
                c("WT", "NT","104","33","398", "180","194","621"),
                c("WT", "NT","1.14180542456425","1.054580427","1.030486181", "-0.25972365","-0.042113329","-0.260525379"),
                c("WT", "NT","high","high","high", "low","low","low"))


#Plot histograms
df = melt(data)
df$variable = as.character(df$variable)
df$Guide = sapply(as.character(df$variable) , GetTreatment)
df$Replicate = sapply(as.character(df$variable) , GetRep)
df$log2FC = sapply(df$Guide, FUN = Assign_Info, y=3)
df$Pos = sapply(df$Guide, FUN = Assign_Info, y=2)
df$Type = sapply(df$Guide, FUN = Assign_Info, y=4)
df$PosR = paste0(df$Guide,"_",df$Replicate)


df$Guide = factor(df$Guide , levels = c("WT", "NT","G100","G101","G102","G103","G104","G105" ))
df$Type = factor(df$Type , levels = c("WT", "NT","low","high" ))
gg1 = ggplot(df, aes(x = value+1, y = Guide)) +
  # facet_wrap(~FullName, ncol = 3)+
  geom_density_ridges(aes(fill = Type)) +
  scale_x_log10(limits=c(10,1000000)) +
  theme_classic()+
  ggtitle("") +
  xlab("GFP signal") +
  ylab("Guide") + 
  scale_fill_manual(values = c("#f0f0f0","#bdbdbd","#de2d26","#3182bd"))

df$PosR = factor(df$PosR , levels = paste0(rep(c("WT", "NT","G100","G101","G102","G103","G104","G105" ) , each = 3), rep(c("_1","_2","_3"), nrow(ID.mat))))
df$Type = factor(df$Type , levels = c("WT", "NT","low","high" ))
gg2 = ggplot(df, aes(x = value+1, y = PosR)) +
  # facet_wrap(~FullName, ncol = 3)+
  geom_density_ridges(aes(fill = Type)) +
  scale_x_log10(limits=c(10,1000000)) +
  theme_classic()+
  ggtitle("") +
  xlab("GFP signal") +
  ylab("Guide") + 
  scale_fill_manual(values = c("#f0f0f0","#bdbdbd","#de2d26","#3182bd"))


lay <- rbind(c(2,1),
             c(2,NA))

# pdf("./FACS_figures/GFPscreenValidation_Histograms.pdf", width = 6, height = 12, useDingbats = F)
# grid.arrange(arrangeGrob(grobs= list(gg1,gg2) ,layout_matrix = lay))
# dev.off()




# determine how many cells are GFP positive (esp. in NT control)
q = quantile(c(data[,grep("WT_1",colnames(data))],data[,grep("WT_2",colnames(data))],data[,grep("WT_3",colnames(data))]), probs = c(0.99))
PercGFPpos = apply(data , 2, CalcPercGFPpos) # determine how many cells got transfected and select the same percentage across all conditions assuming equal efficiency
P = mean(PercGFPpos[grep("NT_",names(PercGFPpos))])
Transfected_cells = apply(data , 2, SelectTopCells, p = P)




# Calculate Mean MFI per sample
data = as.data.frame(Transfected_cells)
data$NT = apply(data[,grep("NT",colnames(data))], MARGIN = 1 , mean)
#MFIs = colMeans(data) # colmeans to get mean mfi per column 
MFIs = apply(data, MARGIN = 2, Calculate_gMFI) # get geometric mean (less affected by outliers)

Reference = "NT"
output.mfi.percent  <- MFIs
for (i in 1:ncol(data)){
  output.mfi.percent[i] <- (MFIs[i]/MFIs[Reference])*100
}


# transform data to data frame, so ggplot can plot it
df <- melt(output.mfi.percent[-grep("NT$",names(output.mfi.percent))])

# Add info to df, to allow ggplot to plot it pretty nicely
df$Guide = sapply(rownames(df) , GetTreatment)
df$GuideSimple = factor(  rep( c("WT","NT","1","2","3","4","5","6") , each = 3) , levels = c("WT","NT","1","2","3","4","5","6"))
df$Replicate = sapply(rownames(df) , GetRep)
df$Type = sapply(df$Guide, FUN = Assign_Info, y=4)
df$Pos = sapply(df$Guide, FUN = Assign_Info, y=2)
df$Screen_l2FC = sapply(df$Guide, FUN = Assign_Info, y=3)
df$Guide = factor(df$Guide , levels = c("WT", "NT","G100","G101","G102","G103","G104","G105" ))
df$Type = factor(df$Type , levels = c("WT", "NT","low","high" ))


g = ggplot( data = df, aes( x = GuideSimple, y = value , color = Type )) + 
  ylab("GFP intensity %") + 
  xlab("guide RNA") +
  geom_point(pch = 21, position = position_jitterdodge(jitter.width = 1.5), size = 2) +
  coord_fixed((8/(110))) + 
  theme_classic() + 
  scale_color_manual(values = c("#252525","#bdbdbd","#de2d26","#3182bd")) +
  scale_y_continuous(breaks=c(seq(0,125,25)) , limits = c(0,135)) + 
  stat_summary(fun.y=mean,fun.ymin=mean, fun.ymax=mean, geom="crossbar", aes(group=Type), position=position_dodge(), size=0.35)+
  geom_vline(xintercept=seq(2,8,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) 


pdf("./FACS_figures/Fig1e_GFPScreenValidation.pdf", width = 3.5, height = 3, useDingbats = F)
grid.arrange(arrangeGrob(grobs= list(g)) )
dev.off()

