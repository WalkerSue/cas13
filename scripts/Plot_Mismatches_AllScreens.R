#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(pheatmap, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(Rsamtools, quietly = T))
suppressMessages(library(GenomicAlignments, quietly = T))
suppressMessages(library(stringr, quietly = T))
suppressMessages(library(RColorBrewer, quietly = T))
suppressMessages(library(ggseqlogo, quietly = T))
suppressMessages(library(ggpubr, quietly = T))


###### sub-routines #################################


GetmmPos <- function(y){
  return( as.integer(strsplit(y, split = "_")[[1]][3] ))
}

GetDeltaCS <- function(x){
  

  # temporarily keep track of rownames
  x$rn <- x$GuideName
  
  x$DeltaCS <- NA
  
  #split by Guide
  GUIDE <- split(x, f = x$Guide)
  
  # for each guide return the delta CS
  for ( i in 1:length(GUIDE)){
    
    # skip non-targeting guides
    if(grepl("rc_0",names(GUIDE)[i]) == T){
      next
    }
    else if(grepl("rc_0",names(GUIDE)[i]) == F){
      #if only the parental guide but no permutation is present, assign 0 and next
      if ( nrow(GUIDE[[i]]) == 1 ){
        GUIDE[[i]]$DeltaCS <- 0
      }
      else if(nrow(GUIDE[[i]]) > 1){
        
        # determine reference guide
        ref <- which(GUIDE[[i]]$MatchType == "Perfect Match")
        if (length(ref) == 1){
          GUIDE[[i]]$DeltaCS = GUIDE[[i]]$meanCS - GUIDE[[i]]$meanCS[ref]
        }
        else if(length(ref) < 1){
          #print(i)
          next
        }
        else{
          stop("Exiting. More than 1 reference guide. Please review code.")
        }
      } 
      else{
        stop("exiting. verify all guides have at least 1 element assigned")
      }
    }
    else{
      stop("exiting. verify guide names")
    }
  }
  
  
  # combine into its original format
  out <- do.call(rbind,GUIDE)
  
  # remove guides with no usable DeltaCS
  del = which(is.na(out$DeltaCS) == TRUE)
  if(length(del) > 0){
    out = out[-del,]
  }
  
  # restore rownames
  rownames(out) <- NULL
  out <- out[,-which(colnames(x) == "rn")]
  
  #return output
  return(out)
} 



TranslatePval <- function(x){
  if( x > 0.05){
    return("")
  } else if (x >= 0.01 & x < 0.05 ){
    return("*")
  } else if (x >= 0.001 & x < 0.01 ){
    return("**")
  } else if ( x < 0.001 ){
    return("***")
  }
}


cor_mm_Pos <- function(x, PLOT=TRUE, TITLE = ""){

  #remove random double mismatches
  x = x[grep("Random Double",x$MatchType, invert = T),]
  
  # extract the mismatch position
  x$mmPos <- sapply( x$GuideName ,GetmmPos)
  
  # split by guide type
  TYPES <- split(x, f = x$MatchType)
  # isolate types all single mismatches
  FO <- TYPES[[grep("First Order",names(TYPES), invert = F)]]
  # CD <- TYPES[[grep("Consecutive Double",names(TYPES), invert = F)]]
  # CT <- TYPES[[grep("Consecutive Triple",names(TYPES), invert = F)]]
  # select corresponding perfect matching guides
  PM <- TYPES[[grep("Perfect Match",names(TYPES), invert = F)]]
  PM.FO <- PM[ which(PM$Guide %in% FO$Guide)  ,]
  # PM.CD <- PM[ which(PM$Guide %in% CD$Guide)  ,]
  # PM.CT <- PM[ which(PM$Guide %in% CT$Guide)  ,]

  # split by mismatch position
  FO.pos <- split(FO, f = FO$mmPos)
  # CD.pos <- split(CD, f = CD$mmPos)
  # CT.pos <- split(CT, f = CT$mmPos)
  # 
  # keep only CRISPR scores
  FO.pos.dCS = lapply(FO.pos, FUN=function(x){x[,"DeltaCS"]})
  # CD.pos.dCS = lapply(CD.pos, FUN=function(x){x[,"DeltaCS"]})
  # CT.pos.dCS = lapply(CT.pos, FUN=function(x){x[,"DeltaCS"]})
  FO.pos = lapply(FO.pos, FUN=function(x){x[,"meanCS"]})
  # CD.pos = lapply(CD.pos, FUN=function(x){x[,"meanCS"]})
  # CT.pos = lapply(CT.pos, FUN=function(x){x[,"meanCS"]})

  
  # # for consecutive double and triple, expand the value to all mismatching nt. Currently the deltalog2foldchange is associated with only the first mismatching nt
  # # consectutive double
  # l = length(CD.pos)
  # for (i in l:1){
  #   if ( i == length(CD.pos)){
  #     CD.pos[[i+1]] <- CD.pos[[i]]
  #     CD.pos.dCS[[i+1]] <- CD.pos.dCS[[i]]
  #     names(CD.pos)[i+1] = "27"
  #     names(CD.pos.dCS)[i+1] = "27"
  #   }else{
  #     CD.pos[[i+1]] <- c( CD.pos[[i]] , CD.pos[[i+1]] )
  #     CD.pos.dCS[[i+1]] <- c( CD.pos.dCS[[i]] , CD.pos.dCS[[i+1]] )
  #   }
  # }
  # # consectutive triple
  # l = length(CT.pos)
  # for (i in l:1){
  #   if ( i == length(CT.pos)){
  #     CT.pos[[i+2]] <- CT.pos[[i]]
  #     CT.pos[[i+1]] <- CT.pos[[i]]
  #     CT.pos.dCS[[i+2]] <- CT.pos.dCS[[i]]
  #     CT.pos.dCS[[i+1]] <- CT.pos.dCS[[i]]
  #     names(CT.pos)[i+1] = "26"
  #     names(CT.pos)[i+2] = "27"
  #     names(CT.pos.dCS)[i+1] = "26"
  #     names(CT.pos.dCS)[i+2] = "27"
  #   }else{
  #     CT.pos[[i+1]] <- c( CT.pos[[i]] , CT.pos[[i+1]] )
  #     CT.pos[[i+2]] <- c( CT.pos[[i]] , CT.pos[[i+2]] )
  #     CT.pos.dCS[[i+1]] <- c( CT.pos.dCS[[i]] , CT.pos.dCS[[i+1]] )
  #     CT.pos.dCS[[i+2]] <- c( CT.pos.dCS[[i]] , CT.pos.dCS[[i+2]] )
  #   }
  # }
  
  # Get positional P value
  P.FO <- vector()
  # P.CD <- vector()
  # P.CT <- vector()
   
  for (j in 1:length(FO.pos)){   P.FO[j] <- signif(t.test(FO.pos[[j]] , PM.FO$meanCS, alternative = "less")$p.value, 2)   }
  p.FO.adj <- p.adjust(P.FO, method = "bonferroni", n = length(P.FO)) # correction for multiple testing
  P.FO.adj <- sapply(p.FO.adj, TranslatePval) # Translate Pval
  
  # for (j in 1:length(CD.pos)){   P.CD[j] <- signif(t.test(CD.pos[[j]] , PM.CD$meanCS, alternative = "less")$p.value, 2)   }
  # p.CD.adj <- p.adjust(P.CD, method = "bonferroni", n = length(P.CD)) # correction for multiple testing
  # P.CD.adj <- sapply(p.CD.adj, TranslatePval) # Translate Pval
  
  # for (j in 1:length(CT.pos)){   P.CT[j] <- signif(t.test(CT.pos[[j]] , PM.CT$meanCS, alternative = "less")$p.value, 2)   }
  # p.CT.adj <- p.adjust(P.CT, method = "bonferroni", n = length(P.CT)) # correction for multiple testing
  # P.CT.adj <- sapply(p.CT.adj, TranslatePval) # Translate Pval
  
  
  
  lbl <- as.data.frame(cbind( names(FO.pos)  ,P.FO.adj )) 
  colnames(lbl ) <- c("nt","value")
  lbl.df <- melt(lbl, id.vars = "nt")
  lbl.df$nt <- as.integer(as.character(lbl.df$nt))
  
  df <- melt(FO.pos.dCS)
  colnames(df)[2] = "mmPos"
  df$mmPos <- as.numeric(as.character(df$mmPos))
  #df <- melt(pos[,grep("mmPos|DeltaCS.BIN1",colnames(pos))], id.vars = "mmPos")
  
  FO.means = (aggregate(value ~ mmPos, df, mean))
  FO.medians = (aggregate(value ~ mmPos, df, median))
  
  n = max(table(df$mmPos))
  
  y=2.6
  R = (length(table(df$mmPos))/(2*y))
  
  
  

  g1<-ggplot(df,aes(x=mmPos,y=value)) + 
    geom_boxplot(aes(group=mmPos),fill = "#f0f0f0",outlier.shape=20, outlier.size=0.01) + 
    # geom_jitter( shape = 4, size = 0.5, width = 0.2, colour = "darkgrey") +
    geom_smooth(method = 'loess' ,na.rm=T, span = 0.5) +
    ylim(c(-y,y)) +
    #xlim(c(28,0)) + 
    theme_classic() + ylab("delta log2foldchange") + xlab("guide position [nt]") +  ggtitle(paste0(TITLE," : single mismatches (n=",n,")")) + 
    theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = R/1.8) +
    geom_hline(yintercept=0, linetype="solid", color = "black") + 
    theme(legend.position = "none") + 
    geom_text(data = lbl.df  ,mapping = aes(x = nt, y = 1.25, label = value, angle = 90), size=5, vjust = 0.9 ) +
    scale_x_continuous(breaks = 28:0, labels=c("",as.character(c(27:1)),""))
  
  

  
  
  
  
  
  # lbl <- as.data.frame(cbind( names(CD.pos)  ,P.CD.adj )) 
  # colnames(lbl ) <- c("nt","value")
  # lbl.df <- melt(lbl, id.vars = "nt")
  # lbl.df$nt <- as.integer(as.character(lbl.df$nt))
  # 
  # 
  # df <- melt(CD.pos.dCS)
  # colnames(df)[2] = "mmPos"
  # df$mmPos <- as.numeric(as.character(df$mmPos))
  # 
  # CD.means = (aggregate(value ~ mmPos, df, mean))
  # CD.medians = (aggregate(value ~ mmPos, df, median))
  # 
  # g2<- ggplot(df,aes(x=mmPos,y=value)) + 
  #   geom_boxplot(aes(group=mmPos),fill = "#f0f0f0", outlier.shape=20, outlier.size=0.01) + 
  #   # geom_jitter( shape = 4, size = 0.5, width = 0.2, colour = "darkgrey") +
  #   geom_smooth(method = 'loess' ,na.rm=T, span = 0.5) +
  #   ylim(c(-2,2)) +
  #   #xlim(c(28,0)) + 
  #   theme_classic() + ylab("delta log2foldchange") + xlab("guide position [nt]") +  ggtitle("consecutive double mismatches") + 
  #   theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = 3) +
  #   geom_hline(yintercept=0, linetype="solid", color = "black") + 
  #   theme(legend.position = "none") + 
  #   geom_text(data = lbl.df  ,mapping = aes(x = nt, y = 1.25, label = value, angle = 90), size=5, vjust = 0.9 ) + 
  #   scale_x_continuous(breaks = 28:0, labels=c("",as.character(c(27:1)),""))
  # 
  # 
  # 
  # 
  # 
  # lbl <- as.data.frame(cbind( names(CT.pos)  ,P.CT.adj )) 
  # colnames(lbl ) <- c("nt","value")
  # lbl.df <- melt(lbl, id.vars = "nt")
  # lbl.df$nt <- as.integer(as.character(lbl.df$nt))
  # 
  # 
  # df <- melt(CT.pos.dCS)
  # colnames(df)[2] = "mmPos"
  # df$mmPos <- as.numeric(as.character(df$mmPos))
  # 
  # CT.means = (aggregate(value ~ mmPos, df, mean))
  # CT.medians = (aggregate(value ~ mmPos, df, median))
  # 
  # g3<- ggplot(df,aes(x=mmPos,y=value)) + 
  #   geom_boxplot(aes(group=mmPos),fill = "#f0f0f0", outlier.shape=20, outlier.size=0.01) + 
  #   # geom_jitter( shape = 4, size = 0.5, width = 0.2, colour = "darkgrey") +
  #   geom_smooth(method = 'loess' ,na.rm=T, span = 0.25) +
  #   ylim(c(-2,2)) + 
  #   #xlim(c(28,0)) + 
  #   theme_classic() + ylab("delta log2foldchange") + xlab("guide position [nt]") +  ggtitle("consecutive triple mismatches") + 
  #   theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = 3) +
  #   geom_hline(yintercept=0, linetype="solid", color = "black") + 
  #   theme(legend.position = "none") + 
  #   geom_text(data = lbl.df  ,mapping = aes(x = nt, y = 1.25, label = value, angle = 90), size=5, vjust = 0.9 ) + 
  #   scale_x_continuous(breaks = 28:0, labels=c("",as.character(c(27:1)),""))
  
  # ma = cbind(FO.means,CD.means[,2],CT.means[,2],FO.medians[,2],CD.medians[,2],CT.medians[,2])
  # colnames(ma) = c("mmPos","FO.mean","CD.mean","CT.mean","FO.median","CD.median","CT.median")
  
  return(g1)
}




########################## sub-routines end  ########

###### execute ######################################


#load data
# Read combined result table
Results = read.delim('./data/CombinedTilingScreenResults.csv' , sep = ',', header = T, stringsAsFactors = F)

# Only use CDS annotating guides (can be scwitched off)
Results = Results[grep("CDS",Results$Annotation),]

# keep only relevant categories
Results = Results[which(Results$MatchType %in% c("Perfect Match","First Order","Random Double") ),]

# Restrict to single Screens
Screens = split(Results, f = Results$Screen)
GFP = Screens[["GFP"]]
CD46 = Screens[["CD46"]]
CD55 = Screens[["CD55"]]
CD71 = Screens[["CD71"]]




##################  Get deltaCS
GFP <-  GetDeltaCS(GFP)
CD46 <-  GetDeltaCS(CD46)
CD55 <-  GetDeltaCS(CD55)
CD71 <-  GetDeltaCS(CD71)



##################  Analyze straight positional effects
# Transform data and plot
p = list()
p[[1]] = cor_mm_Pos(GFP, TITLE = "GFP")
p[[2]] = cor_mm_Pos(CD46, TITLE = "CD46")
p[[3]] = cor_mm_Pos(CD55, TITLE = "CD55")
p[[4]] = cor_mm_Pos(CD71, TITLE = "CD71")

pdf("./figures/FigS4x.pdf", width = 5, height = 12, useDingbats = F)
grid.arrange(arrangeGrob(grobs= p,ncol=1))
dev.off()

