#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(preprocessCore, quietly = T))



###### sub-routines #################################

normalizeFC = function(dat = GFP , all = All , VALUE = "meanCS"){
  UQd = quantile(dat$meanCS , na.rm = T , probs = 0.75)
  LQd = quantile(dat$meanCS , na.rm = T , probs = 0.25)
  UQp = quantile(all$meanCS , na.rm = T , probs = 0.75)
  LQp = quantile(all$meanCS , na.rm = T , probs = 0.25)
  
  out = sapply( dat[,VALUE], FUN = function(x){  return(((x - LQd) / (UQd - LQd)) * (UQp - LQp) + LQp)  })
  return(out)
}

########################## sub-routines end  ########

###### execute ######################################

# Read combined result table
Results = read.delim('./data/CombinedTilingScreenResults.csv' , sep = ',', header = T, stringsAsFactors = F)

# Only use CDS annotating guides (can be scwitched off)
Results = Results[grep("CDS",Results$Annotation),]

# Perfect Matches only
Results = Results[which(Results$MatchType == "Perfect Match"),]

# Scale Data Jointly
Results$ScaledCS.all = scale(Results$meanCS , center = T)[,1]

# Restrict to single Screens
Screens = split(Results, f = Results$Screen)
GFP = Screens[["GFP"]]
CD46 = Screens[["CD46"]]
CD55 = Screens[["CD55"]]
CD71 = Screens[["CD71"]]

# Scale Data individually
GFP$ScaledCS = scale(GFP$meanCS , center = T)[,1]
CD46$ScaledCS = scale(CD46$meanCS , center = T)[,1]
CD55$ScaledCS = scale(CD55$meanCS , center = T)[,1]
CD71$ScaledCS = scale(CD71$meanCS , center = T)[,1]
All = rbind.data.frame(GFP,CD46,CD55,CD71)

# Normalize as in Agarwal 2018
GFP$normCS = normalizeFC(dat = GFP , all = All , VALUE = "meanCS")
CD46$normCS = normalizeFC(dat = CD46 , all = All , VALUE = "meanCS")
CD55$normCS = normalizeFC(dat = CD55 , all = All , VALUE = "meanCS")
CD71$normCS = normalizeFC(dat = CD71 , all = All , VALUE = "meanCS")
All = rbind.data.frame(GFP,CD46,CD55,CD71)
ModelInput = rbind.data.frame(GFP,CD46,CD55,CD71)
del = which( is.na(ModelInput$meanCS) == TRUE )
if(length(del) > 0){
  ModelInput = ModelInput[-del,]
}

write.table(ModelInput , file = './data/NormalizedCombinedTilingScreenResults.csv', sep=',', quote = FALSE)


### Each screen exhibited a variable level of global guide enrichment. Reasons for this variability presumably included variability in sorting efficiency and differences in either the target abundance (TA) or the target protein halflife. 
### Because we did not have the power in sample size to accurately model the effects, we normalized the screens to the same scale prior to training and testing the model. 
### To do so, for each screen D, we computed the upper and lower quartiles of the mRNA log2 fold changes (UQd and LQd, respectively) as well as the corresponding quartiles for the log2 fold changes among all datasets pooled together (UQp and LQp). 
### We then updated each log2 fold change x as follows: x` = ((x - LQd) / (UQd - LQd)) * (UQp - LQp) + LQp)
### By centering on quartiles, this procedure normalized the fold-change distributions in a way that was less susceptible to the influence of outliers.


# 
# # transform
# All = All[,which(colnames(All) %in% c("Screen" , "meanCS", "ScaledCS.all" , "ScaledCS", "normCS" ))]
# 
# p = list()
# for( i in c("meanCS", "ScaledCS.all" , "ScaledCS", "normCS" )){
#   p[[i]] = ggplot(All, aes( x = Screen,  y = All[,i] , fill = Screen)) +
#     ylab(i)+
#     geom_violin() +
#     theme_classic()+
#     theme(legend.position='none')
#   pdf(paste0('./figures/_Test_Norm_',i,'.pdf') , width = 3, height = 3, useDingbats = F)
#   print(p[[i]])
#   dev.off()
# }
# 
# 
# # Get top 20th percentule
# S = split(All , f = All$Screen)
# q1 = quantile(All$ScaledCS , probs = 0.8, na.rm = T)
# q2 = quantile(All$normCS , probs = 0.8, na.rm = T)
# 
# n.cs = table(All[ which(All$ScaledCS >= q1),"Screen"])
# n.csnorm = table(All[ which(All$normCS >= q2),"Screen"])
# n = table(All[ ,"Screen"])
# 
# 
# s = sapply( S , FUN = function(x){quantile(x[,"meanCS"], probs=0.8, na.rm = T)})
# g0 = ggplot(All, aes( x = Screen,  y = meanCS , fill = Screen)) +
#   geom_violin() +
#   theme_classic()+
#   ylab("meanCS")+
#   annotate("text" , x = 1 , y = s[1]+0.2 , vjust = 0 , label = paste0("top20%"))+
#   annotate("text" , x = c(1:4) , y = rep(-1.5,4) , label = paste0("n=", n))+
#   annotate("segment" ,  x = seq(0.7,3.7,1) , xend = seq(1.3,4.3,1), y = s, yend = s )
# 
# 
# 
# 
# s = sapply( S , FUN = function(x){quantile(x[,"ScaledCS"], probs=0.8, na.rm = T)})
# g1 = ggplot(All, aes( x = Screen,  y = ScaledCS , fill = Screen)) +
#   geom_violin() +
#   theme_classic()+
#   ylab("scaled (individually)")+
#   annotate("text" , x = 1 , y = s[1]+0.2 , vjust = 0 , label = paste0("top20%"))+
#   annotate("text" , x = c(1:4) , y = rep(-3.5,4) , label = paste0("n=", n))+
#   annotate("segment" ,  x = seq(0.7,3.7,1) , xend = seq(1.3,4.3,1), y = s, yend = s )
# 
# s = sapply( S , FUN = function(x){quantile(x[,"normCS"], probs=0.8, na.rm = T)})
# g2 = ggplot(All, aes( x = Screen,  y = normCS , fill = Screen)) +
#   geom_violin() +
#   theme_classic()+
#   ylab("normalized")+
#   annotate("text" , x = 1 , y = s[1]+0.2 , vjust = 0 , label = paste0("top20%"))+
#   annotate("text" , x = c(1:4) , y = rep(-1.25,4) , label = paste0("n=", n))+
#   annotate("segment" ,  x = seq(0.7,3.7,1) , xend = seq(1.3,4.3,1), y = s, yend = s )
# 
# Y = sum(abs(range(All$ScaledCS , na.rm = T)))
# X = sum(abs(range(All$normCS , na.rm = T)))
# R = X/Y
# 
# g3 = ggplot(All, aes( x = normCS,  y = ScaledCS , color = Screen)) +
#   geom_point(shape = 20, size = 1) +
#   theme_classic()+
#   coord_fixed(ratio = R) +
#   ylab("scaled")+
#   xlab("normalized")
# 
# 
# 
# 
# 
# Individual = cbind.data.frame(as.data.frame(n.cs/n * 100) , rep("individually", length(n)))
# colnames(Individual)[3] = "scaled"
# Joint = cbind.data.frame(as.data.frame(n.csnorm/n * 100), rep("normalized", length(n)))
# colnames(Joint)[3] = "scaled"
# df = rbind.data.frame(Individual , Joint )
# colnames(df)[1] = "Screen"
# 
# g4 = ggplot(df , aes( x = scaled , y = Freq , fill = Screen)) +
#   geom_bar( stat = 'identity', position = 'dodge') +
#   theme_classic()+
#   ylab("%")+
#   ggtitle("selected from indiv. screens")
# 
# 
# 
# Individual = cbind.data.frame(as.data.frame(n.cs/sum(n.cs) * 100) , rep("ind.", length(n)))
# colnames(Individual)[3] = "scaled"
# Joint = cbind.data.frame(as.data.frame(n.csnorm/sum(n.csnorm) * 100), rep("norm.", length(n)))
# colnames(Joint)[3] = "scaled"
# DF = rbind.data.frame(Individual , Joint )
# colnames(DF)[1] = "Screen"
# 
# g5 = ggplot(DF , aes( x = scaled , y = Freq , fill = Screen)) +
#   geom_bar( stat = 'identity', position = 'stack')+
#   theme_classic()+
#   ylab("%")+
#   ggtitle("representation")
# 
# 
# lay = rbind(c(1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,6,6),
#             c(1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,6,6))
# pdf("./figures/Fig_ScalingRepresentation.pdf", width = 21, height = 3.5, useDingbats = F)
# grid.arrange(grobs = list(g0,g1,g2,g3,g4,g5) , layout_matrix = lay)
# dev.off()





