#!/bin/bash

DIRs=$(ls ./data/ | grep -E "ENST000")


for dir in $DIRs;do 
 N=$(ls ./data/${dir}/*.fasta | wc -l)
 O=$(ls ./data/${dir}/*.csv | wc -l)
 echo -e "$dir $O of $N"
done 

 N=$(ls ./data/ENST*/*.fasta | wc -l)
 O=$(ls ./data/ENST*/*.csv | wc -l)
 echo -e "\nOverall $O of $N\n"